package hu.adsd;

import io.javalin.Javalin;

import java.util.ArrayList;

public class App {
    public static void main(String[] args) {
        Javalin app = Javalin.create(/*config*/)
                .get("/", ctx -> ctx.result("Hello World"))
                .start(7070);
        ArrayList<WeatherInfo> cities =  new ArrayList<>();
        cities.add(new WeatherInfo("Amersfoort", 23));
        cities.add(new WeatherInfo("Amsteram", 22));
        cities.add(new WeatherInfo("Den Haag", 24));
        cities.add(new WeatherInfo("Rotterdam", 23));

        app.get("/cities", ctx -> {
          ctx.json(cities);
        });

        app.get("/weather/{name}", ctx -> {
           String name = ctx.pathParam("name");
           for (WeatherInfo weatherInfo : cities)
           {
               if (weatherInfo.getName().equalsIgnoreCase(name))
               {
                   ctx.status(200);
                   ctx.json(weatherInfo);
                   return;
               }
           }
           ctx.status(404);
           ctx.result("Stad niet gevonden!");
        });
    }
}
