package hu.adsd;

public class WeatherInfo {
    private String name;
    private int temperature;

    public WeatherInfo(String name, int temperature) {
        this.name = name;
        this.temperature = temperature;
    }

    public String getName() {
       return this.name;
    }

    public int getTemperature() {
        return this.temperature;
    }
}
